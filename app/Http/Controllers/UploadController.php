<?php

namespace App\Http\Controllers;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class UploadController extends Controller
{
    public function index()
    {
        $files = File::all();
        return view('upload.index', ['files' => $files]);
    }

    // public function index(int $id)
    // {
    //     // すべてのフォルダを取得する
    //     $folders = Folder::all();

    //     // 選ばれたフォルダを取得する
    //     $current_folder = Folder::find($id);

    //     // 選ばれたフォルダに紐づくタスクを取得する
    //     $tasks = Task::where('folder_id', $current_folder->id)->get();

    //     return view('tasks/index', [
    //         'folders' => $folders,
    //         'current_folder_id' => $current_folder->id,
    //         'tasks' => $tasks,
    //     ]);
    // }
    

    public function store(Request $request)
    {
        $files = File::all();

        $file = new File();
        $file->name = $request->name;
        
        if ($request->hasfile('image')){
            
            $path = "app/".$request->file('image')->store('/');
            $file->image = basename($path);
        
        }else{
            return $request;
            $file->image = '';
        }
        
        $file->save();
        return redirect()->route('upload.index', [
            'file' => $file,
        ]);
    }





//     return redirect()->back();
//    }
    // public function showEditForm(int $id, int $task_id)
    // {
    //     $task = Task::find($task_id);

    //     return view('tasks/edit', [
    //         'task' => $task,
    //     ]);
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(int $id, int $task_id, EditTask $request)
    // {
    //     // 1
    //     $task = Task::find($task_id);

    //     // 2
    //     $task->title = $request->title;
    //     $task->status = $request->status;
    //     $task->due_date = $request->due_date;
    //     $task->save();

    //     // 3
    //     return redirect()->route('tasks.index', [
    //         'id' => $task->folder_id,
    //     ]);
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }
}
