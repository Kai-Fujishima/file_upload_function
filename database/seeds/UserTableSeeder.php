<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'kai',
            'email' => 'kai@test.com',
            //'email_verified_at',
            'password' => 'kaikaitest',
            'avatar_filename' => 'kaikaiavater',
        ]);
    }
}
