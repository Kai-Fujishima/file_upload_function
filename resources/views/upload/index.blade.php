<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<header><h1>File Uploader</h1></header><br>
    
    <div class="container">
        <div class="jumbotron">
            <form action="{{ url('upload/store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter Name">
                </div>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input">
                        <label class="custom-file-label">File</label>
                    </div>
                </div><br>
                <button type="submit" name="submit" class="btn btn-primary">save data</button>
            </form>

        <div class="row">
            <div class="col col-md-4">
                <nav class="panel panel-default">
                    <div class="panel-heading">画像一覧</div>
                    <div class="panel-body">
                       
                    </div>
                    <div class="list-group">
                        @foreach($files as $file)
                            <img class='logo' width=400px src="{{ asset('storage/' . $file->image) }}" >
                            <p>{{ $file->name }}</p>
                        @endforeach
                    </div>
                </nav>
            </div>
        </div>
        
        <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/zqUeszhcm2Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12961.064808927418!2d139.79368952004086!3d35.69506631741829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5d956183b1684870!2z44Kt44OD44OB44Oz5Y2X5rW3IOS4oeWbveW6lw!5e0!3m2!1sja!2sjp!4v1590907978038!5m2!1sja!2sjp" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe><br>

        <p class="button">
            <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#100;&#115;&#107;&#115;&#116;&#117;&#112;&#105;&#100;&#119;&#105;&#115;&#101;&#48;&#50;&#64;&#105;&#99;&#108;&#111;&#117;&#100;&#46;&#99;&#111;&#109;">メールする</a>
        </p>


        <img src="/mnt/c/Users/fujis/project/file_upload/storage/app/public/0w2d6akQhLR4KsFPf0wlHqiY9vVtzWxTFoeAGydB.jpeg">-->
        
        <img class='logo' width=400px src="{{ asset('storage/Rz1XlEzrdBDlavKwfG8VKGVAyIZjp2BxTEV7RxIN.jpeg') }}" >
            <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
            <script>
                bsCustomFileInput.init();
            </script>
        </div>
    </div>

</body>
</html>
